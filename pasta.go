package main

import (
	"fmt"
	"net/http"
	"sync"
)

var (
	visits int
	mu     sync.rwmutex
)

const htmltemplate = `
<html>
<body>
    <h1>Welcome, Chirpy Admin</h1>
    <p>Chirpy has been visited %d times!</p>
</body>
</html>
`

func adminMetricsHandler(w http.ResponseWriter, r *http.Request) {
	mu.RLock()
	currentVisits := visits
	mu.RUnlock()

	response := fmt.Sprintf(htmlTemplate, currentVisits)
	w.Header().Set("Content-Type", "text/html")
	w.Write([]byte(response)) // Write the formatted response
}

func resetHandler(w http.ResponseWriter, r *http.Request) {
	mu.Lock()
	visits = 0
	mu.Unlock()
	w.Write([]byte("reset completed"))
}

func main() {
	http.HandleFunc("/admin/metrics", adminMetricsHandler)
	http.HandleFunc("/api/reset", resetHandler)
	http.ListenAndServe(":8080", nil)
}
