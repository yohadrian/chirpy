package main

import (
	// "encoding/json"
	"fmt"
	"html/template"
	"log"
	"net/http"
	"sync"
)

var tmpl = template.Must(template.New("metrics").Parse(htmltemplate))

var (
	visits int
	mu     sync.RWMutex
)

const htmltemplate = `
<html>
<body>
    <h1>Welcome, Chirpy Admin</h1>
    <p>Chirpy has been visited {{.Visits}} times!</p>
</body>
</html>
`

type apiConfig struct {
	fshits int
}

func (ctr *apiConfig) mid(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		ctr.fshits++
		log.Println("Middleware hit")
		next.ServeHTTP(w, r)
	})
}

func (ctr *apiConfig) metricsHandler(w http.ResponseWriter, r *http.Request) {
	log.Println("Metrics Handler hit")
	if r.Method != http.MethodGet {
		w.WriteHeader(http.StatusMethodNotAllowed)
		return
	}
	w.Header().Add("Content-Type", "text/plain; charset=utf-8")
	w.WriteHeader(http.StatusOK)
	w.Write([]byte(fmt.Sprintf("Hits: %d\n", ctr.fshits)))
}

func adminMetricsHandler(w http.ResponseWriter, r *http.Request) {
	mu.Lock()
	log.Println("Admin Metrics Handler hit")
	visits++
	currentVisits := visits
	log.Println("Current visits incremented to:", visits) // Debug logging
	mu.Unlock()

	response := fmt.Sprintf(htmltemplate, currentVisits)
	w.Header().Set("Content-Type", "text/html")
	w.Write([]byte(response)) // Write the formatted response
}

func (ctr *apiConfig) resetHandler(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodGet {
		w.WriteHeader(http.StatusMethodNotAllowed)
		return
	}
	mu.Lock()
	visits = 0
	mu.Unlock()
	w.WriteHeader(http.StatusOK)
}

func (ctr *apiConfig) healthHandler(w http.ResponseWriter, r *http.Request) {
	// Simply respond with "OK" for a GET request.
	if r.Method != http.MethodGet {
		w.WriteHeader(http.StatusMethodNotAllowed)
		return
	}
	w.Header().Add("Content-Type", "text/plain; charset=utf-8")
	w.WriteHeader(http.StatusOK)
	w.Write([]byte(http.StatusText(http.StatusOK)))
}

// func (ctr *apiConfig) validHandler(w http.ResponseWriter, r *http.Request) {
// 	type params struct {
// 		Name string `json:"name"`
// 		Age  int    `json:"age"`
// 	}

// 	decoder

// }

func main() {

	const rootDir = "."
	const port = "8080"

	ctr := apiConfig{
		fshits: 0,
	}

	mux := http.NewServeMux()
	// main
	mux.Handle("/", http.FileServer(http.Dir(rootDir)))
	// app with counter middleware
	mux.Handle("/app/", ctr.mid(http.StripPrefix("/app", http.FileServer(http.Dir(".")))))

	// metrics for counter / reset
	mux.HandleFunc("/api/metrics", ctr.metricsHandler)
	mux.HandleFunc("/api/healthz", ctr.healthHandler)
	mux.HandleFunc("/api/reset", ctr.resetHandler)
	mux.Handle("/admin/metrics", ctr.mid(http.HandlerFunc(adminMetricsHandler)))

	srv := &http.Server{
		Addr:    ":" + port,
		Handler: mux,
	}

	// Start the server
	log.Printf("Listening on :%s on port : %s\n", rootDir, port)
	log.Fatal(srv.ListenAndServe())
}
